<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixture extends Fixture
{

    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 7; $i++) {
            $user = new User();
            $user->setUsername(sprintf('admin%d', $i));
            $user->setPassword($this->encoder->encodePassword($user, 12345));
            $user->setEmail(sprintf('test%d@test.com', $i));
            $manager->persist($user);
        }
        $manager->flush();
    }
}
