<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserVoteAction", mappedBy="user")
     */
    private $userVoteAction;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserCommentAction", mappedBy="user")
     */
    private $user;

    public function __construct()
    {
        $this->userVoteAction = new ArrayCollection();
        $this->user = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }


    /**
     * Returns the roles granted to the user.
     *
     *     public function getRoles()
     *     {
     *         return array('ROLE_USER');
     *     }
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        return [
          'ROLE_USER'
        ];
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return Collection|UserVoteAction[]
     */
    public function getUserVoteAction(): Collection
    {
        return $this->userVoteAction;
    }

    public function addUserVoteAction(UserVoteAction $userVoteAction): self
    {
        if (!$this->userVoteAction->contains($userVoteAction)) {
            $this->userVoteAction[] = $userVoteAction;
            $userVoteAction->setUser($this);
        }

        return $this;
    }

    public function removeUserVoteAction(UserVoteAction $userVoteAction): self
    {
        if ($this->userVoteAction->contains($userVoteAction)) {
            $this->userVoteAction->removeElement($userVoteAction);
            // set the owning side to null (unless already changed)
            if ($userVoteAction->getUser() === $this) {
                $userVoteAction->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserCommentAction[]
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(UserCommentAction $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user[] = $user;
            $user->setUser($this);
        }

        return $this;
    }

    public function removeUser(UserCommentAction $user): self
    {
        if ($this->user->contains($user)) {
            $this->user->removeElement($user);
            // set the owning side to null (unless already changed)
            if ($user->getUser() === $this) {
                $user->setUser(null);
            }
        }

        return $this;
    }
}
