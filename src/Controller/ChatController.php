<?php
/**
 * Created by PhpStorm.
 * User: elshan
 * Date: 12/17/18
 * Time: 2:30 PM
 */

namespace App\Controller;


use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ChatController extends AbstractController
{

    /**
     * @Route("/chat", name="app_chat")
     */

    public function index(UserRepository $user)
    {
//        dd($user->findAll());
//        return $this->render('chat/index.html.twig', ['users' => $user->findAll()]);
        return $this->render('chat/index_chat3.html.twig', ['users' => $user->findAll()]);
    }

}