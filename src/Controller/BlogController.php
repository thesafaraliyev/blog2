<?php
/**
 * Created by PhpStorm.
 * User: elshan
 * Date: 10/31/18
 * Time: 9:32 AM
 */

namespace App\Controller;


use App\Entity\Article;
use App\Entity\User;
use App\Entity\UserVoteAction;
use App\Repository\ArticleRepository;
use App\Repository\UserCommentActionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class BlogController extends AbstractController
{

    /**
     * @Route("/", name="app_homepage")
     */

    public function homePage(EntityManagerInterface $entityManager)
    {
        $data = $entityManager->getRepository(Article::class)->findAll();
        return $this->render('blogTemps/homepage.html.twig', [
            'data' => $data
        ]);
    }

    /**
     * @param $slug
     * @Route("delete/{slug}", name="delete_article")
     * @Method({"DELETE"})
     */
    public function deleteArticle(Article $article)
    {
        $del = $this->getDoctrine()->getManager();
        $del->remove($article);
        $del->flush();

        return new JsonResponse(['status' => 'ok']);
    }

    /**
     * @Route("add/newArticle", name="add_new_article")
     */
    public function addNewArticle(Request $request, UserInterface $user)
    {
        $article = new Article();
        $form = $this->createFormBuilder($article, ['allow_extra_fields' => true])
            ->add('title', TextType::class, ['attr' => ['class' => 'form-control']])
            ->add('slug', TextType::class, ['attr' => ['class' => 'form-control']])
            ->add('content', TextareaType::class, ['required' => false, 'attr' => ['class' => 'form-control contentTextbox']])
            ->add('save', SubmitType::class, ['label' => 'Create', 'attr' => ['class' => 'btn btn-sm btn-primary mt-5']])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $article = $form->getData();
            $em = $this->getDoctrine()->getManager();

            $author = $user->getUsername();
            $rep = $this->getDoctrine()->getRepository(User::class);
            $userId = $rep->findOneBy(['username' => $author]);
            $article->setCreatedAt(new \DateTime());
            $article->setAuthor($userId->getId());
            $em->persist($article);

            $em->flush();

            return $this->redirectToRoute('app_homepage');
        }

        return $this->render('blogTemps/addNewArticle.html.twig', [
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("edit_article/{slug}", name="edit_article")
     */
    public function editArticle(Request $request, $slug)
    {
        $article = $this->getDoctrine()->getRepository(Article::class)->findOneBy(['slug' => $slug]);
        $form = $this->createFormBuilder($article, ['allow_extra_fields' => true])
            ->add('title', TextType::class, ['attr' => ['class' => 'form-control']])
            ->add('slug', TextType::class, ['attr' => ['class' => 'form-control']])
            ->add('content', TextareaType::class, ['required' => false, 'attr' => ['class' => 'form-control contentTextbox']])
            ->add('save', SubmitType::class, ['label' => 'Update', 'attr' => ['class' => 'btn btn-sm btn-primary mt-5']])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('app_homepage');
        }

        return $this->render('blogTemps/editArticle.html.twig', [
            'form' => $form->createView()
        ]);
    }


    /**
     * @param $slug
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("show/{slug}", name="show_article")
     */
    public function showArticle(Article $article, ArticleRepository $articleRepository, UserCommentActionRepository $userCommentAction)
    {
        $comments   = $userCommentAction->findAllCommentsByArticleId($article);
        $articleId  = $article->getId();
        $rep2       = $this->getDoctrine()->getRepository(UserVoteAction::class);
        $allLikes   = $rep2->countArticleLikes($article);
        $userLiked  = true;

        if ($this->getUser()) {
            $userId = $this->getUser()->getId();
            $getUserLike = $rep2->findLikeByUserIdAndArticleId($userId, $articleId);
            if ($getUserLike) {
                $userLiked = false;
            }
        }

        return $this->render('blogTemps/showArticle.html.twig', [
            'article'   => $article,
            'likes'     => $allLikes,
            'userLiked' => $userLiked,
            'comments' => $comments
        ]);
    }
    
}