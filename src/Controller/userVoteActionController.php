<?php
/**
 * Created by PhpStorm.
 * User: elsha
 * Date: 11/5/2018
 * Time: 11:35 PM
 */

namespace App\Controller;


use App\Entity\Article;
use App\Entity\UserVoteAction;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class userVoteActionController extends AbstractController
{
    /**
     * @Route("/add_remove_like/{slug}", name="app_toggle_like")
     */
    public function toggleLike(Article $article, EntityManagerInterface $manager)
    {
        $userId     = $this->getUser()->getId();
        $articleId  = $article->getId();

        $rep        = $this->getDoctrine()->getRepository(UserVoteAction::class);
        $like       = $rep->findLikeByUserIdAndArticleId($userId, $articleId);
        $action     = new UserVoteAction();

        if (!$like) {
            $action->setUser($this->getUser());
            $action->setArticle($article->getId());
            $manager->persist($action);

        } else {
            $manager->remove($like[0]);
        }
        $manager->flush();

        $allLikes = $rep->countArticleLikes($articleId);
        return new JsonResponse(['hearts' => $allLikes]);
    }
}