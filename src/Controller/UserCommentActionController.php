<?php
/**
 * Created by PhpStorm.
 * User: elshan
 * Date: 11/6/18
 * Time: 12:23 PM
 */

namespace App\Controller;


use App\Entity\Article;
use App\Entity\UserCommentAction;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserCommentActionController extends AbstractController
{
    /**
     * @param Article $article
     * @return JsonResponse
     * @Route("/add_new_comment/{slug}", name="new_comment_add")
     */
    public function addUserComment(Article $article, Request $request, EntityManagerInterface $entityManager)
    {
        $articleId  = $article->getId();
        $newComment = $request->get('comment');
        $flag       = false;

        if ($newComment) {
            $comment = new UserCommentAction();
            $comment->setUser($this->getUser());
            $comment->setArticle($articleId);
            $comment->setComment($newComment);
            $entityManager->persist($comment);
            $entityManager->flush();
            $flag = true;
        }
        return new JsonResponse(['status' => $flag]);
    }
}