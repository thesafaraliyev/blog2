<?php
/**
 * Created by PhpStorm.
 * User: elshan
 * Date: 11/5/18
 * Time: 5:41 PM
 */

namespace App\Controller;


use App\Entity\Article;
use App\Entity\Userlikes;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class LikeController extends AbstractController
{
    /**
     * @Route("toggle_article_like/{slug}", name="toggle_like")
     */
    public function addOrDeleteArticleLike($slug, Request $request)
    {
        $em = $this->getDoctrine()->getRepository(Article::class);
        /** @var Article $article */
        $article = $em->findOneBy(['slug' => $slug])->getId();

        if (!$request->getMethod() == 'POST' || !$article) {
            throw $this->createNotFoundException('Article not found!');
        }

        $user = $this->getUser();
        $rep2 = $this->getDoctrine()->getRepository(Userlikes::class);

        if ($this->getUser()) {
            $userId         = $user->getId();

            $getUserLike    = $rep2->findLikeByUserIdAndArticleId($userId, $article);
            $likes          = new Userlikes();
            $em             = $this->getDoctrine()->getManager();

            if (!$getUserLike) {
                $likes->setArticle($article)->setUser($userId);
                $em->persist($likes);

            } else {
                $likeId = $rep2->findOneBy(['id' => $getUserLike[0]->getId()]);
                $em->remove($likeId);

            }
            $em->flush();
        }

        $allLikes = $rep2->countArticleLikes($article);
        return new JsonResponse(['hearts' => $allLikes]);
    }
}