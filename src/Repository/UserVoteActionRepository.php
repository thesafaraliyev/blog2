<?php

namespace App\Repository;

use App\Entity\UserVoteAction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserVoteAction|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserVoteAction|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserVoteAction[]    findAll()
 * @method UserVoteAction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserVoteActionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserVoteAction::class);
    }

//    /**
//     * @return UserVoteAction[] Returns an array of UserVoteAction objects
//     */
    public function findLikeByUserIdAndArticleId($userId, $articleId)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.user = :userId')
            ->andWhere('u.article = :articleId')
            ->setParameter('userId', $userId)
            ->setParameter('articleId', $articleId)
            ->getQuery()
            ->getResult()
            ;
    }

    public function countArticleLikes($article)
    {
        return $this->createQueryBuilder('u')
            ->select('count(u.id)')
            ->andWhere('u.article = :articleId')
            ->setParameter('articleId', $article)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /*
    public function findOneBySomeField($value): ?UserVoteAction
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
