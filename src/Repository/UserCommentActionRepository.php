<?php

namespace App\Repository;

use App\Entity\UserCommentAction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserCommentAction|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserCommentAction|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserCommentAction[]    findAll()
 * @method UserCommentAction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserCommentActionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserCommentAction::class);
    }

//    /**
//     * @return UserCommentAction[] Returns an array of UserCommentAction objects
//     */

    public function findAllCommentsByArticleId($articleId)
    {
        return $this->createQueryBuilder('u')
            ->innerJoin('u.user', 'user')
            ->andWhere('u.article = :val')
            ->addSelect('user.username')
            ->addSelect('u.comment')
            ->setParameter('val', $articleId)
            ->getQuery()
            ->getResult()
        ;
    }


    /*
    public function findOneBySomeField($value): ?UserCommentAction
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
