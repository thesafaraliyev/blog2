let express = require('./public/node_modules/express');
let app     = express();
let server  = require('http').createServer(app);
let io      = require('./public/node_modules/socket.io').listen(server);

let users = [];
let conns = [];

server.listen(process.env.PORT || 3000);
console.log('server running...');

app.get('/', (req, res) => {
    res.sendFile(__dirname + './templates/chat/index.html.twig');
});

io.on('connection', function(socket){
    // connect
    console.log('a user connected');

    // disconnect
    socket.on('disconnect', function(){
        users.splice(users.indexOf(socket.username), 1);
        updateUserNames();
        console.log('user disconnected');
    });



    // send message

    socket.on('send message', data => {
        console.log(data);
        io.sockets.emit('new message', { msg: data, user: socket.username })
    });

    // new user

    socket.on('new user', (data, callback) => {
        if (users.indexOf(data) != 1) {
            callback(false);
        } else {
            callback(true);
            socket.username = data;
            users.push(socket.username);
            updateUserNames();
        }
    });

    let updateUserNames = () => {
        io.sockets.emit('get users', users)
    };


    let total = io.engine.clientsCount;
    // console.log(total);
    // socket.emit('getCount', io.engine.clientsCount)

});




