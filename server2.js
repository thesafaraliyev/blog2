let express = require('./public/node_modules/express');
let app     = express();
let server  = require('http').createServer(app);
let io      = require('./public/node_modules/socket.io').listen(server);

let users = {};


server.listen(process.env.PORT || 3000);
console.log('server running...');

app.get('/', (req, res) => {
    res.sendFile(__dirname + './templates/chat/index.html.twig');
});

//put up socket functionality on server side
io.sockets.on('connection', function (socket) {//everytime a user connects has its own socket
    //console.log("new user");
    socket.on('new user', function (data, callback) {
        console.log("New user");
        //socket.emit('select_room',data);
        if (data in users) {//if index of is not -1..i.e nickname exist
            callback(false);
        } else {
            console.log("here");
            callback(true);
            socket.nickname = data;//store nickname of each user becomes clear on disconnect
            users[socket.nickname] = socket;//key value pair as defined above
            //nicknames.push(socket.nickname);
            //io.sockets.emit('usernames',nicknames);//send usernames for display
            updateNicknames();
        }
    });
    socket.on('sendmessage', function (data, callback) {
        console.log(data);
        var msg = data.trim();
        if (msg[0] == '@')//if thats whisper or private msg
        {
            msg = msg.substr(1);//start of name onwards
            console.log(msg)
            var idx = msg.indexOf(' ');
            if (idx !== -1) {
                //check the username is valid
                var name = msg.substr(0, idx);
                msg = msg.substr(idx + 1);
                if (name in users) {
                    users[name].emit('whisper', {msg: msg, nick: socket.nickname});
                    console.log('whispered');
                }
                else {
                    callback('user is not active');
                }
            }
            else//no actual msg part
            {
                callback('Error! Please enter a message for your whisper');
            }
        }
        else {
            io.sockets.emit('newmessage', {msg: msg, nick: socket.nickname});//broadcast to everyone and i too can see the msg
            //socket.broadcast.emit('newmessage',data);//broadcast to evry1 except me
        }

    });

    function updateNicknames() {
        io.sockets.emit('usernames', Object.keys(users));//sending socket does not make sense
    }

    //whenever user disconnect he/she should be removed from the list
    socket.on('disconnect', function (data) {
        if (!socket.nickname)//when the user has no nickname
            return;
        delete users[socket.nickname];
        updateNicknames();
    });

    socket.on('typing', data => {
        console.log(data)
        socket.broadcast.emit('typing', { username: socket.nickname })
    })
});