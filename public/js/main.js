$(function(){
    $('.btn-outline-danger').on('click', function (e) {
        let article = $(this).data('slug');
        let isConfirmed = confirm("Are you sure?");
        if (isConfirmed) {
            deleteArticle(article);
        }
    });

    $('.comment-add').on('click', function (e) {
        let commentText = $('.new-comment').val().trim();
        let articleSlug = $('.container').data('slug');
        if (commentText) {
            addComment(commentText, articleSlug);
        }
    });


    function deleteArticle(data)
    {
        url = '/delete/' + data;
        $.ajax({
            method: "POST",
            url: url
        }).done(function (data) {
            if (data.status === 'ok') {
                location.reload()
            }
        })
    }


    function addComment(comment, slug)
    {
        url = '/add_new_comment/' + slug;
        $.ajax({
            method: "POST",
            data: { comment: comment },
            url: url
        }).done(function (data) {
            if (data.status === true) {
                location.reload()
            }
        })
    }


    $('.like-article').on('click', function (e) {
        e.preventDefault();
        let likeButton = $('.article-like-icon ');
        let link = $(e.currentTarget);

        if (likeButton.hasClass('far')) {
            likeButton.toggleClass('far').toggleClass('fas');
        } else {
            likeButton.toggleClass('fas').toggleClass('far');
        }

        $.ajax({
            method: 'POST',
            url: link.attr('href')
        }).done(function(data) {
            $('.like-counter').html(data.hearts);
        })

    })


});
